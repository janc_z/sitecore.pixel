﻿using Sitecore.Pixel.Feature.BasicComponent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sitecore.Pixel.Feature.BasicComponent.Controllers
{
    public class BasicComponentController : Controller
    {
        // GET: BasicComponent
        public ActionResult Index()
        {
            var model = new BasicComponentModel();
            return View("BasicComponent.cshtml", model);
        }
    }
}