﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Pixel.Feature.BasicComponent.Models
{
    public class BasicComponentModel
    {
        public string Title { get; set; }

        public string Subtitle { get; set; }

        public string Image { get; set; }

        public DateTime DateCreated { get; set; }

        public List<AuthorModel> Authors { get; set; }
    }
}