﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Pixel.Feature.BasicComponent.Models
{
    public class AuthorModel
    {
        public string FullName { get; set; }

        public string Bio { get; set; }

        public string LinkToPage { get; set; }

        public string Avatar { get; set; }
    }
}