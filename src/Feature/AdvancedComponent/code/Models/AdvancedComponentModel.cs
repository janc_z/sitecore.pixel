﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Pixel.Feature.BasicComponent.Models;

namespace Sitecore.Pixel.Feature.AdvancedComponent.Models
{
    public class AdvancedComponentModel : BasicComponentModel
    {
        public string AditionalInfo { get; set; }
    }
}