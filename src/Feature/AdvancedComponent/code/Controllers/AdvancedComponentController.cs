﻿using Sitecore.Data;
using Sitecore.Pixel.Feature.AdvancedComponent.Models;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sitecore.Pixel.Feature.AdvancedComponent.Controllers
{
    public class AdvancedComponentController : Controller
    {
        // GET: AdvancedComponent
        public ActionResult Index()
        {
            var model = new AdvancedComponentModel();

            var contextItem = Sitecore.Context.Database.GetItem(new ID("{22BE23CD-1F21-44B4-8BCE-AD05A67E91E0}"));

            model.Title = contextItem.Fields["Title"].Value;
            model.Subtitle = contextItem.Fields["Subtitle"].Value;
            //model.DateCreated = new DateTime(contextItem.Fields["DateCreated"].Value);
            //model.Authors = contextItem.Fields["Authors"];
            model.Image = MediaManager.GetMediaUrl(contextItem.Fields["Image"].Item);
            model.AditionalInfo = contextItem.Fields["AditionalInfo"].Value;

            return View("AdvancedComponent", model);
        }
    }
}